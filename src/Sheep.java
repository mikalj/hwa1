public class Sheep {

   enum Animal {sheep, goat};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Animal[] animals) {
      int goatCounter = 0;
      for (int i = 0; i < animals.length; i++) {
         if (animals[i] == Animal.goat) {
            animalPutter(animals, i, Animal.sheep);
            animalPutter(animals, goatCounter, Animal.goat);
            goatCounter++;
         }
      }
   }
   public static void animalPutter(Animal[] animals, int animalIndex, Animal animal){
      animals[animalIndex] = animal;
   }
}
